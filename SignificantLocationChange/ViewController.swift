//
//  ViewController.swift
//  SignificantLocationChange
//
//  Created by Ashutosh Singh on 03/01/24.
//

import UIKit
import CoreLocation

class ViewController: UIViewController,LocationManagerDelegate, UITableViewDelegate, UITableViewDataSource{
    
    var locations: [CLLocation] = []
    @IBOutlet weak var cout: UILabel!
    @IBOutlet weak var locationTavleView: UITableView!
    let locationManager = LocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
//        locationManager.delegate = self
//        locationManager.startMonitoringSignificantLocationChanges()
      
    }
    @IBAction func showPopUp(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewFileVC") as! NewFileVC
        self.addChild(viewController)
        self.view.addSubview(viewController.view)
        viewController.didMove(toParent: self)
        
    }
    @IBAction func alertView(_ sender: Any) {
        let overLayerView = OverLayerView()
        overLayerView.appear(sender: self)
    }
    
    
    
    func locationManagerDidUpdateLocations(_ locationManager: LocationManager, newLocation: CLLocation) {
        
        locations.append(newLocation)
        if locations.count > 0{
            cout.text =  "Location: \(locations.count)"
        }else{
            cout.text =  "Location"
        }
        locationTavleView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationChangeCell", for: indexPath) as? LocationChangeCell
        let location = locations[indexPath.row]
        
        // Corrected: Update both latitude and longitude labels
        cell?.latLable.text = "\(location.coordinate.latitude)"
        cell?.longLabel.text = "\(location.coordinate.longitude)"
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//     Define the editing style for the cell (delete in this case)
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the selected location
            locations.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            if locations.count > 0{
                cout.text =  "Location: \(locations.count)"
            }else{
                cout.text =  "Location"
            }
           
        }
    }
}

