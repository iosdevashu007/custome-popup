//
//  LocationManager.swift
//  SignificantLocationChange
//
//  Created by Ashutosh Singh on 03/01/24.
//

import Foundation
import CoreLocation
import UIKit


class LocationManager: NSObject, CLLocationManagerDelegate {
    private let locationManager = CLLocationManager()
    
    var delegate: LocationManagerDelegate?


    override init() {
        super.init()

        // Set up the location manager
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.requestAlwaysAuthorization()
    }

    func startMonitoringSignificantLocationChanges() {
        locationManager.startMonitoringSignificantLocationChanges()
    }

    // CLLocationManagerDelegate methods

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            print("New location: \(location.coordinate.latitude), \(location.coordinate.longitude)")
            delegate?.locationManagerDidUpdateLocations(self, newLocation: location)
            // Handle the new location data
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager error: \(error.localizedDescription)")
    }
  
}

protocol LocationManagerDelegate: AnyObject {
    func locationManagerDidUpdateLocations(_ locationManager: LocationManager, newLocation: CLLocation)
}

struct Locations {
    var latitude: Double?
    var longitude: Double?
}

