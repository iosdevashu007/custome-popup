//
//  NewFileVC.swift
//  SignificantLocationChange
//
//  Created by Ashutosh Singh on 29/01/24.
//

import UIKit

class NewFileVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.mainView.layer.cornerRadius = 10
        addAnimation()
        

        // Do any additional setup after loading the view.
    }
    @IBAction func hidePopUp(_ sender: Any) {
        removePopUp()
    }
    
    func addAnimation(){
        self.mainView.transform = CGAffineTransform(translationX: 0, y: self.mainView.frame.height)
        UIView.animate(withDuration: 0.3, animations: {
            self.mainView.transform = .identity
        })
    }
    
    func removePopUp(){
        
        self.mainView.transform = .identity
        UIView.animate(withDuration: 0.3, animations: {
            self.mainView.transform = CGAffineTransform(translationX: 0, y: self.mainView.frame.height)
        }) { (completion) in
            self.view.removeFromSuperview()
        }
        
    }
    

}
